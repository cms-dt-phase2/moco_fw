--=================================================================================================--
-- Module for communication with the GBTX chip             
-- Adapted from https://gitlab.cern.ch/gbtsc-fpga-support/gbtsc-example-design
-- By Javier Sastre Alvaro (javier.sastre@ciemat.es), Alvaro Navarro Tobar, Paolo de Remigis
--=================================================================================================--

-- IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- Custom libraries and packages:
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_moco_gbt.all;
use work.tm7_gbt_decl.all;
use work.gbt_bank_package.all;
use work.bo2.all;
use work.mp7_ttc_decl.all;

entity moco_gbt is
  port (   
    clk40               : in std_logic; -- Frame clock
    -- vc707_gbt_example_design uses 156 MHz for DRP/SYSCLK, we replace it by 100 MHz 
    clk100              : in std_logic; -- Fabric clock 100 MHz   
    -- MGT --
-- MGT reference clock (from external Si5338 clock generator), 120MHz for the
-- latency-optimized GBT Bank
    mgt117_clkp, mgt117_clkn    : in  std_logic;
    mgt117_txp, mgt117_txn      : out std_logic_vector(0 to 3);
    mgt117_rxp, mgt117_rxn      : in  std_logic_vector(0 to 3);
    mgt118_clkp, mgt118_clkn    : in  std_logic;
    mgt118_txp, mgt118_txn      : out std_logic_vector(0 to 3);
    mgt118_rxp, mgt118_rxn      : in  std_logic_vector(0 to 3);
    mgt119_clkp, mgt119_clkn    : in  std_logic;
    mgt119_txp, mgt119_txn      : out std_logic_vector(0 to 3);
    mgt119_rxp, mgt119_rxn      : in  std_logic_vector(0 to 3);
    
    -- ipbus --
    ipb_clk             : in std_logic;
    ipb_rst             : in std_logic;
    ipb_in              : in ipb_wbus;
    ipb_out             : out ipb_rbus;
    
    ----------------
    -- data ports --
    ----------------
    ttc_cmd             : in std_logic_vector(7 downto 0);
    bunch_ctr           : in std_logic_vector(11 downto 0);
    headerLock          : out std_logic;
    txFrameClk_lock     : out std_logic;
    mgtrefclk_rst       : out std_logic
    
  );
end entity;

architecture structural of moco_gbt is

  signal clk40_dly              : std_logic;
  signal txFrameClk_from_txPll  : std_logic;
  signal rxData_s, txData_s     : gbt_reg84_A(1 to 12);
  signal rxIsData_s, txIsData_s : std_logic_vector(1 to 12);
  signal rxWordClk_s            : std_logic_vector(1 to 12);
  signal header_lock_v          : std_logic_vector(1 to 12);

  -- IPBUS BRANCHES
  signal ipb_to_slaves  : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);

  -- IPBUS REGISTER
  constant N_STAT : integer := 64;
  constant N_CTRL : integer := 64;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat, csrd: ipb_reg_v(N_STAT-1 downto 0);
  signal conf, csrq: ipb_reg_v(N_CTRL-1 downto 0);
  -----------------------------
  -- Aggregate signals that basically vectorize the per-link confs, stats, and strobes
  -----------------------------
  type slv8_v_t is array(natural range <>) of std_logic_vector(7 downto 0);
  type slv12_v_t is array(natural range <>) of std_logic_vector(11 downto 0);
  type slv32_v_t is array(natural range <>) of std_logic_vector(31 downto 0);
  type usgn32_v_t is array(natural range <>) of unsigned(31 downto 0);
  type usgn24_v_t is array(natural range <>) of unsigned(23 downto 0);
  -- Conf for txData_s
  signal erase_elink_word : std_logic_vector(1 to 12);
  signal txdata_sel       : std_logic_vector(1 to 12);
  signal elink_fire       : std_logic_vector(1 to 12);
  signal tp_fire          : std_logic_vector(1 to 12);
  signal bunchctr_fire    : slv12_v_t       (1 to 12);
  signal tp_bctr          : slv12_v_t       (1 to 12);
  signal txdata_reg       : gbt_reg84_A     (1 to 12);
  signal last_elink_word  : slv32_v_t       (1 to 12);
  signal last_elink_byte  : slv8_v_t        (1 to 12);
  signal unlock_ctr       : usgn24_v_t      (1 to 12);
  signal unlock_time      : usgn32_v_t      (1 to 12);

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_frameClk_shift, conf_link10_elink_fire, conf_link10_erase_elink_word, conf_link10_tp_fire,
    conf_link10_txdata_sel, conf_link11_elink_fire, conf_link11_erase_elink_word, conf_link11_tp_fire,
    conf_link11_txdata_sel, conf_link12_elink_fire, conf_link12_erase_elink_word, conf_link12_tp_fire,
    conf_link12_txdata_sel, conf_link1_elink_fire, conf_link1_erase_elink_word, conf_link1_tp_fire,
    conf_link1_txdata_sel, conf_link2_elink_fire, conf_link2_erase_elink_word, conf_link2_tp_fire,
    conf_link2_txdata_sel, conf_link3_elink_fire, conf_link3_erase_elink_word, conf_link3_tp_fire,
    conf_link3_txdata_sel, conf_link4_elink_fire, conf_link4_erase_elink_word, conf_link4_tp_fire,
    conf_link4_txdata_sel, conf_link5_elink_fire, conf_link5_erase_elink_word, conf_link5_tp_fire,
    conf_link5_txdata_sel, conf_link6_elink_fire, conf_link6_erase_elink_word, conf_link6_tp_fire,
    conf_link6_txdata_sel, conf_link7_elink_fire, conf_link7_erase_elink_word, conf_link7_tp_fire,
    conf_link7_txdata_sel, conf_link8_elink_fire, conf_link8_erase_elink_word, conf_link8_tp_fire,
    conf_link8_txdata_sel, conf_link9_elink_fire, conf_link9_erase_elink_word, conf_link9_tp_fire,
    conf_link9_txdata_sel, conf_mgtrefclk_rst, conf_resetgbtfpga, stat_frameClk_lock : std_logic := '0';

  signal conf_frameClk_shift_count, stat_link10_last_elink_byte, stat_link11_last_elink_byte,
    stat_link12_last_elink_byte, stat_link1_last_elink_byte, stat_link2_last_elink_byte, stat_link3_last_elink_byte,
    stat_link4_last_elink_byte, stat_link5_last_elink_byte, stat_link6_last_elink_byte, stat_link7_last_elink_byte,
    stat_link8_last_elink_byte, stat_link9_last_elink_byte : std_logic_vector(7 downto 0) := (others => '0');
  signal conf_link10_bunchctr_fire, conf_link10_tp_bctr, conf_link11_bunchctr_fire, conf_link11_tp_bctr,
    conf_link12_bunchctr_fire, conf_link12_tp_bctr, conf_link1_bunchctr_fire, conf_link1_tp_bctr,
    conf_link2_bunchctr_fire, conf_link2_tp_bctr, conf_link3_bunchctr_fire, conf_link3_tp_bctr,
    conf_link4_bunchctr_fire, conf_link4_tp_bctr, conf_link5_bunchctr_fire, conf_link5_tp_bctr,
    conf_link6_bunchctr_fire, conf_link6_tp_bctr, conf_link7_bunchctr_fire, conf_link7_tp_bctr,
    conf_link8_bunchctr_fire, conf_link8_tp_bctr, conf_link9_bunchctr_fire,
    conf_link9_tp_bctr : std_logic_vector(11 downto 0) := (others => '0');
  signal stat_link10_last_elink_word, stat_link11_last_elink_word, stat_link12_last_elink_word,
    stat_link1_last_elink_word, stat_link2_last_elink_word, stat_link3_last_elink_word, stat_link4_last_elink_word,
    stat_link5_last_elink_word, stat_link6_last_elink_word, stat_link7_last_elink_word, stat_link8_last_elink_word,
    stat_link9_last_elink_word : std_logic_vector(31 downto 0) := (others => '0');
  signal conf_link10_txdata_reg, conf_link11_txdata_reg, conf_link12_txdata_reg, conf_link1_txdata_reg,
    conf_link2_txdata_reg, conf_link3_txdata_reg, conf_link4_txdata_reg, conf_link5_txdata_reg, conf_link6_txdata_reg,
    conf_link7_txdata_reg, conf_link8_txdata_reg,
    conf_link9_txdata_reg : std_logic_vector(83 downto 0) := (others => '0');

  signal conf_elink_sample_bit : unsigned(2 downto 0) := (others => '0');
  signal stat_link10_unlock_ctr, stat_link11_unlock_ctr, stat_link12_unlock_ctr, stat_link1_unlock_ctr,
    stat_link2_unlock_ctr, stat_link3_unlock_ctr, stat_link4_unlock_ctr, stat_link5_unlock_ctr, stat_link6_unlock_ctr,
    stat_link7_unlock_ctr, stat_link8_unlock_ctr, stat_link9_unlock_ctr : unsigned(23 downto 0) := (others => '0');
  signal stat_link10_unlock_time, stat_link11_unlock_time, stat_link12_unlock_time, stat_link1_unlock_time,
    stat_link2_unlock_time, stat_link3_unlock_time, stat_link4_unlock_time, stat_link5_unlock_time,
    stat_link6_unlock_time, stat_link7_unlock_time, stat_link8_unlock_time,
    stat_link9_unlock_time : unsigned(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin


  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_mgtrefclk_rst                   <= conf(16#00#)(0);
  conf_resetgbtfpga                    <= conf(16#00#)(1);
  conf_elink_sample_bit                <= unsigned( conf(16#00#)(4 downto 2) );
  conf_frameClk_shift                  <= conf(16#00#)(5);
  conf_frameClk_shift_count            <= conf(16#00#)(13 downto 6);
  conf_link1_erase_elink_word          <= conf(16#04#)(0);
  conf_link1_txdata_sel                <= conf(16#04#)(1);
  conf_link1_elink_fire                <= conf(16#04#)(2);
  conf_link1_tp_fire                   <= conf(16#04#)(3);
  conf_link1_bunchctr_fire             <= conf(16#04#)(15 downto 4);
  conf_link1_tp_bctr                   <= conf(16#04#)(27 downto 16);
  conf_link1_txdata_reg(31 downto 0)   <= conf(16#05#)(31 downto 0);
  conf_link1_txdata_reg(63 downto 32)  <= conf(16#06#)(31 downto 0);
  conf_link1_txdata_reg(83 downto 64)  <= conf(16#07#)(19 downto 0);
  conf_link2_erase_elink_word          <= conf(16#08#)(0);
  conf_link2_txdata_sel                <= conf(16#08#)(1);
  conf_link2_elink_fire                <= conf(16#08#)(2);
  conf_link2_tp_fire                   <= conf(16#08#)(3);
  conf_link2_bunchctr_fire             <= conf(16#08#)(15 downto 4);
  conf_link2_tp_bctr                   <= conf(16#08#)(27 downto 16);
  conf_link2_txdata_reg(31 downto 0)   <= conf(16#09#)(31 downto 0);
  conf_link2_txdata_reg(63 downto 32)  <= conf(16#0A#)(31 downto 0);
  conf_link2_txdata_reg(83 downto 64)  <= conf(16#0B#)(19 downto 0);
  conf_link3_erase_elink_word          <= conf(16#0C#)(0);
  conf_link3_txdata_sel                <= conf(16#0C#)(1);
  conf_link3_elink_fire                <= conf(16#0C#)(2);
  conf_link3_tp_fire                   <= conf(16#0C#)(3);
  conf_link3_bunchctr_fire             <= conf(16#0C#)(15 downto 4);
  conf_link3_tp_bctr                   <= conf(16#0C#)(27 downto 16);
  conf_link3_txdata_reg(31 downto 0)   <= conf(16#0D#)(31 downto 0);
  conf_link3_txdata_reg(63 downto 32)  <= conf(16#0E#)(31 downto 0);
  conf_link3_txdata_reg(83 downto 64)  <= conf(16#0F#)(19 downto 0);
  conf_link4_erase_elink_word          <= conf(16#10#)(0);
  conf_link4_txdata_sel                <= conf(16#10#)(1);
  conf_link4_elink_fire                <= conf(16#10#)(2);
  conf_link4_tp_fire                   <= conf(16#10#)(3);
  conf_link4_bunchctr_fire             <= conf(16#10#)(15 downto 4);
  conf_link4_tp_bctr                   <= conf(16#10#)(27 downto 16);
  conf_link4_txdata_reg(31 downto 0)   <= conf(16#11#)(31 downto 0);
  conf_link4_txdata_reg(63 downto 32)  <= conf(16#12#)(31 downto 0);
  conf_link4_txdata_reg(83 downto 64)  <= conf(16#13#)(19 downto 0);
  conf_link5_erase_elink_word          <= conf(16#14#)(0);
  conf_link5_txdata_sel                <= conf(16#14#)(1);
  conf_link5_elink_fire                <= conf(16#14#)(2);
  conf_link5_tp_fire                   <= conf(16#14#)(3);
  conf_link5_bunchctr_fire             <= conf(16#14#)(15 downto 4);
  conf_link5_tp_bctr                   <= conf(16#14#)(27 downto 16);
  conf_link5_txdata_reg(31 downto 0)   <= conf(16#15#)(31 downto 0);
  conf_link5_txdata_reg(63 downto 32)  <= conf(16#16#)(31 downto 0);
  conf_link5_txdata_reg(83 downto 64)  <= conf(16#17#)(19 downto 0);
  conf_link6_erase_elink_word          <= conf(16#18#)(0);
  conf_link6_txdata_sel                <= conf(16#18#)(1);
  conf_link6_elink_fire                <= conf(16#18#)(2);
  conf_link6_tp_fire                   <= conf(16#18#)(3);
  conf_link6_bunchctr_fire             <= conf(16#18#)(15 downto 4);
  conf_link6_tp_bctr                   <= conf(16#18#)(27 downto 16);
  conf_link6_txdata_reg(31 downto 0)   <= conf(16#19#)(31 downto 0);
  conf_link6_txdata_reg(63 downto 32)  <= conf(16#1A#)(31 downto 0);
  conf_link6_txdata_reg(83 downto 64)  <= conf(16#1B#)(19 downto 0);
  conf_link7_erase_elink_word          <= conf(16#1C#)(0);
  conf_link7_txdata_sel                <= conf(16#1C#)(1);
  conf_link7_elink_fire                <= conf(16#1C#)(2);
  conf_link7_tp_fire                   <= conf(16#1C#)(3);
  conf_link7_bunchctr_fire             <= conf(16#1C#)(15 downto 4);
  conf_link7_tp_bctr                   <= conf(16#1C#)(27 downto 16);
  conf_link7_txdata_reg(31 downto 0)   <= conf(16#1D#)(31 downto 0);
  conf_link7_txdata_reg(63 downto 32)  <= conf(16#1E#)(31 downto 0);
  conf_link7_txdata_reg(83 downto 64)  <= conf(16#1F#)(19 downto 0);
  conf_link8_erase_elink_word          <= conf(16#20#)(0);
  conf_link8_txdata_sel                <= conf(16#20#)(1);
  conf_link8_elink_fire                <= conf(16#20#)(2);
  conf_link8_tp_fire                   <= conf(16#20#)(3);
  conf_link8_bunchctr_fire             <= conf(16#20#)(15 downto 4);
  conf_link8_tp_bctr                   <= conf(16#20#)(27 downto 16);
  conf_link8_txdata_reg(31 downto 0)   <= conf(16#21#)(31 downto 0);
  conf_link8_txdata_reg(63 downto 32)  <= conf(16#22#)(31 downto 0);
  conf_link8_txdata_reg(83 downto 64)  <= conf(16#23#)(19 downto 0);
  conf_link9_erase_elink_word          <= conf(16#24#)(0);
  conf_link9_txdata_sel                <= conf(16#24#)(1);
  conf_link9_elink_fire                <= conf(16#24#)(2);
  conf_link9_tp_fire                   <= conf(16#24#)(3);
  conf_link9_bunchctr_fire             <= conf(16#24#)(15 downto 4);
  conf_link9_tp_bctr                   <= conf(16#24#)(27 downto 16);
  conf_link9_txdata_reg(31 downto 0)   <= conf(16#25#)(31 downto 0);
  conf_link9_txdata_reg(63 downto 32)  <= conf(16#26#)(31 downto 0);
  conf_link9_txdata_reg(83 downto 64)  <= conf(16#27#)(19 downto 0);
  conf_link10_erase_elink_word         <= conf(16#28#)(0);
  conf_link10_txdata_sel               <= conf(16#28#)(1);
  conf_link10_elink_fire               <= conf(16#28#)(2);
  conf_link10_tp_fire                  <= conf(16#28#)(3);
  conf_link10_bunchctr_fire            <= conf(16#28#)(15 downto 4);
  conf_link10_tp_bctr                  <= conf(16#28#)(27 downto 16);
  conf_link10_txdata_reg(31 downto 0)  <= conf(16#29#)(31 downto 0);
  conf_link10_txdata_reg(63 downto 32) <= conf(16#2A#)(31 downto 0);
  conf_link10_txdata_reg(83 downto 64) <= conf(16#2B#)(19 downto 0);
  conf_link11_erase_elink_word         <= conf(16#2C#)(0);
  conf_link11_txdata_sel               <= conf(16#2C#)(1);
  conf_link11_elink_fire               <= conf(16#2C#)(2);
  conf_link11_tp_fire                  <= conf(16#2C#)(3);
  conf_link11_bunchctr_fire            <= conf(16#2C#)(15 downto 4);
  conf_link11_tp_bctr                  <= conf(16#2C#)(27 downto 16);
  conf_link11_txdata_reg(31 downto 0)  <= conf(16#2D#)(31 downto 0);
  conf_link11_txdata_reg(63 downto 32) <= conf(16#2E#)(31 downto 0);
  conf_link11_txdata_reg(83 downto 64) <= conf(16#2F#)(19 downto 0);
  conf_link12_erase_elink_word         <= conf(16#30#)(0);
  conf_link12_txdata_sel               <= conf(16#30#)(1);
  conf_link12_elink_fire               <= conf(16#30#)(2);
  conf_link12_tp_fire                  <= conf(16#30#)(3);
  conf_link12_bunchctr_fire            <= conf(16#30#)(15 downto 4);
  conf_link12_tp_bctr                  <= conf(16#30#)(27 downto 16);
  conf_link12_txdata_reg(31 downto 0)  <= conf(16#31#)(31 downto 0);
  conf_link12_txdata_reg(63 downto 32) <= conf(16#32#)(31 downto 0);
  conf_link12_txdata_reg(83 downto 64) <= conf(16#33#)(19 downto 0);
  stat(16#00#)(0)                      <= stat_frameClk_lock;
  stat(16#04#)(31 downto 0)            <= stat_link1_last_elink_word;
  stat(16#05#)(7 downto 0)             <= stat_link1_last_elink_byte;
  stat(16#05#)(31 downto 8)            <= std_logic_vector( stat_link1_unlock_ctr );
  stat(16#06#)(31 downto 0)            <= std_logic_vector( stat_link1_unlock_time );
  stat(16#08#)(31 downto 0)            <= stat_link2_last_elink_word;
  stat(16#09#)(7 downto 0)             <= stat_link2_last_elink_byte;
  stat(16#09#)(31 downto 8)            <= std_logic_vector( stat_link2_unlock_ctr );
  stat(16#0A#)(31 downto 0)            <= std_logic_vector( stat_link2_unlock_time );
  stat(16#0C#)(31 downto 0)            <= stat_link3_last_elink_word;
  stat(16#0D#)(7 downto 0)             <= stat_link3_last_elink_byte;
  stat(16#0D#)(31 downto 8)            <= std_logic_vector( stat_link3_unlock_ctr );
  stat(16#0E#)(31 downto 0)            <= std_logic_vector( stat_link3_unlock_time );
  stat(16#10#)(31 downto 0)            <= stat_link4_last_elink_word;
  stat(16#11#)(7 downto 0)             <= stat_link4_last_elink_byte;
  stat(16#11#)(31 downto 8)            <= std_logic_vector( stat_link4_unlock_ctr );
  stat(16#12#)(31 downto 0)            <= std_logic_vector( stat_link4_unlock_time );
  stat(16#14#)(31 downto 0)            <= stat_link5_last_elink_word;
  stat(16#15#)(7 downto 0)             <= stat_link5_last_elink_byte;
  stat(16#15#)(31 downto 8)            <= std_logic_vector( stat_link5_unlock_ctr );
  stat(16#16#)(31 downto 0)            <= std_logic_vector( stat_link5_unlock_time );
  stat(16#18#)(31 downto 0)            <= stat_link6_last_elink_word;
  stat(16#19#)(7 downto 0)             <= stat_link6_last_elink_byte;
  stat(16#19#)(31 downto 8)            <= std_logic_vector( stat_link6_unlock_ctr );
  stat(16#1A#)(31 downto 0)            <= std_logic_vector( stat_link6_unlock_time );
  stat(16#1C#)(31 downto 0)            <= stat_link7_last_elink_word;
  stat(16#1D#)(7 downto 0)             <= stat_link7_last_elink_byte;
  stat(16#1D#)(31 downto 8)            <= std_logic_vector( stat_link7_unlock_ctr );
  stat(16#1E#)(31 downto 0)            <= std_logic_vector( stat_link7_unlock_time );
  stat(16#20#)(31 downto 0)            <= stat_link8_last_elink_word;
  stat(16#21#)(7 downto 0)             <= stat_link8_last_elink_byte;
  stat(16#21#)(31 downto 8)            <= std_logic_vector( stat_link8_unlock_ctr );
  stat(16#22#)(31 downto 0)            <= std_logic_vector( stat_link8_unlock_time );
  stat(16#24#)(31 downto 0)            <= stat_link9_last_elink_word;
  stat(16#25#)(7 downto 0)             <= stat_link9_last_elink_byte;
  stat(16#25#)(31 downto 8)            <= std_logic_vector( stat_link9_unlock_ctr );
  stat(16#26#)(31 downto 0)            <= std_logic_vector( stat_link9_unlock_time );
  stat(16#28#)(31 downto 0)            <= stat_link10_last_elink_word;
  stat(16#29#)(7 downto 0)             <= stat_link10_last_elink_byte;
  stat(16#29#)(31 downto 8)            <= std_logic_vector( stat_link10_unlock_ctr );
  stat(16#2A#)(31 downto 0)            <= std_logic_vector( stat_link10_unlock_time );
  stat(16#2C#)(31 downto 0)            <= stat_link11_last_elink_word;
  stat(16#2D#)(7 downto 0)             <= stat_link11_last_elink_byte;
  stat(16#2D#)(31 downto 8)            <= std_logic_vector( stat_link11_unlock_ctr );
  stat(16#2E#)(31 downto 0)            <= std_logic_vector( stat_link11_unlock_time );
  stat(16#30#)(31 downto 0)            <= stat_link12_last_elink_word;
  stat(16#31#)(7 downto 0)             <= stat_link12_last_elink_byte;
  stat(16#31#)(31 downto 8)            <= std_logic_vector( stat_link12_unlock_ctr );
  stat(16#32#)(31 downto 0)            <= std_logic_vector( stat_link12_unlock_time );

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------


  
--------------------------------------
--------------------------------------
-- IPbus register
--------------------------------------
--------------------------------------

-- ipbus address decode
fabric: entity work.ipbus_fabric_sel
  generic map(
  	NSLV => N_SLAVES,
  	SEL_WIDTH => IPBUS_SEL_WIDTH)
  port map(
    ipb_in => ipb_in,
    ipb_out => ipb_out,
    sel => ipbus_sel_moco_gbt(ipb_in.ipb_addr),
    ipb_to_slaves => ipb_to_slaves,
    ipb_from_slaves => ipb_from_slaves
  );

sync_reg: entity work.ipbus_syncreg_v
generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
port map(
  clk => ipb_clk,
  rst => ipb_rst,
  ipb_in => ipb_to_slaves(N_SLV_MOCO),
  ipb_out => ipb_from_slaves(N_SLV_MOCO),
  slv_clk => clk40,
  d => csrd,
  q => csrq,
  --rstb => stat_stb,
  stb => open
);

conf <= csrq;
csrd <= stat;

mgtrefclk_rst <= conf_mgtrefclk_rst;
txFrameClk_lock <= stat_frameClk_lock;


-- Frame clock delay
txFrameclkDelay_inst: entity work.xlx_k7v7_tx_phaligner
  Port map( 
    -- Reset
    RESET_IN              => '0',
    -- Clocks
    CLK_IN                => clk40,
    CLK_OUT               => clk40_dly,
    -- Control
    SHIFT_IN              => conf_frameClk_shift,
    SHIFT_COUNT_IN        => conf_frameClk_shift_count,
    -- Status
    LOCKED_OUT            => stat_frameClk_lock
  );





erase_elink_word <= ( conf_link1_erase_elink_word  , conf_link2_erase_elink_word  , conf_link3_erase_elink_word  , conf_link4_erase_elink_word  , conf_link5_erase_elink_word  , conf_link6_erase_elink_word  , conf_link7_erase_elink_word  , conf_link8_erase_elink_word  , conf_link9_erase_elink_word  , conf_link10_erase_elink_word , conf_link11_erase_elink_word , conf_link12_erase_elink_word  );
txdata_sel       <= ( conf_link1_txdata_sel        , conf_link2_txdata_sel        , conf_link3_txdata_sel        , conf_link4_txdata_sel        , conf_link5_txdata_sel        , conf_link6_txdata_sel        , conf_link7_txdata_sel        , conf_link8_txdata_sel        , conf_link9_txdata_sel        , conf_link10_txdata_sel       , conf_link11_txdata_sel       , conf_link12_txdata_sel        );
elink_fire       <= ( conf_link1_elink_fire        , conf_link2_elink_fire        , conf_link3_elink_fire        , conf_link4_elink_fire        , conf_link5_elink_fire        , conf_link6_elink_fire        , conf_link7_elink_fire        , conf_link8_elink_fire        , conf_link9_elink_fire        , conf_link10_elink_fire       , conf_link11_elink_fire       , conf_link12_elink_fire        );
tp_fire          <= ( conf_link1_tp_fire           , conf_link2_tp_fire           , conf_link3_tp_fire           , conf_link4_tp_fire           , conf_link5_tp_fire           , conf_link6_tp_fire           , conf_link7_tp_fire           , conf_link8_tp_fire           , conf_link9_tp_fire           , conf_link10_tp_fire          , conf_link11_tp_fire          , conf_link12_tp_fire           );
bunchctr_fire    <= ( conf_link1_bunchctr_fire     , conf_link2_bunchctr_fire     , conf_link3_bunchctr_fire     , conf_link4_bunchctr_fire     , conf_link5_bunchctr_fire     , conf_link6_bunchctr_fire     , conf_link7_bunchctr_fire     , conf_link8_bunchctr_fire     , conf_link9_bunchctr_fire     , conf_link10_bunchctr_fire    , conf_link11_bunchctr_fire    , conf_link12_bunchctr_fire     );
tp_bctr          <= ( conf_link1_tp_bctr           , conf_link2_tp_bctr           , conf_link3_tp_bctr           , conf_link4_tp_bctr           , conf_link5_tp_bctr           , conf_link6_tp_bctr           , conf_link7_tp_bctr           , conf_link8_tp_bctr           , conf_link9_tp_bctr           , conf_link10_tp_bctr          , conf_link11_tp_bctr          , conf_link12_tp_bctr           );
txdata_reg       <= ( conf_link1_txdata_reg        , conf_link2_txdata_reg        , conf_link3_txdata_reg        , conf_link4_txdata_reg        , conf_link5_txdata_reg        , conf_link6_txdata_reg        , conf_link7_txdata_reg        , conf_link8_txdata_reg        , conf_link9_txdata_reg        , conf_link10_txdata_reg       , conf_link11_txdata_reg       , conf_link12_txdata_reg        );

( stat_link1_last_elink_word  , stat_link2_last_elink_word  , stat_link3_last_elink_word  , stat_link4_last_elink_word  , stat_link5_last_elink_word  , stat_link6_last_elink_word  , stat_link7_last_elink_word  , stat_link8_last_elink_word  , stat_link9_last_elink_word  , stat_link10_last_elink_word , stat_link11_last_elink_word , stat_link12_last_elink_word ) <= last_elink_word;
( stat_link1_last_elink_byte  , stat_link2_last_elink_byte  , stat_link3_last_elink_byte  , stat_link4_last_elink_byte  , stat_link5_last_elink_byte  , stat_link6_last_elink_byte  , stat_link7_last_elink_byte  , stat_link8_last_elink_byte  , stat_link9_last_elink_byte  , stat_link10_last_elink_byte , stat_link11_last_elink_byte , stat_link12_last_elink_byte ) <= last_elink_byte;
( stat_link1_unlock_ctr       , stat_link2_unlock_ctr       , stat_link3_unlock_ctr       , stat_link4_unlock_ctr       , stat_link5_unlock_ctr       , stat_link6_unlock_ctr       , stat_link7_unlock_ctr       , stat_link8_unlock_ctr       , stat_link9_unlock_ctr       , stat_link10_unlock_ctr      , stat_link11_unlock_ctr      , stat_link12_unlock_ctr      ) <= unlock_ctr;
( stat_link1_unlock_time      , stat_link2_unlock_time      , stat_link3_unlock_time      , stat_link4_unlock_time      , stat_link5_unlock_time      , stat_link6_unlock_time      , stat_link7_unlock_time      , stat_link8_unlock_time      , stat_link9_unlock_time      , stat_link10_unlock_time     , stat_link11_unlock_time     , stat_link12_unlock_time     ) <= unlock_time;


gen_txdata: for i in 1 to 12 generate

  process(clk40_dly)
    variable pipeout, pipein : std_logic_vector(32 downto 0);
    variable elink_fire_old : std_logic;
    variable tp_fire_old : std_logic;
    variable tp_pending : natural range 1 downto 0;
    variable last_header_lock : std_logic := '0';
  begin
    if rising_edge(txFrameClk_from_txPll) then
      -- Transmit data
      if txdata_sel(i) = '0' then
        txData_s(i)(79 downto 8) <= (others => '0');
        txData_s(i)(7) <= txData_reg(i)(7);
        txData_s(i)(5) <= txData_reg(i)(5);
        txData_s(i)(4) <= bo2sl(ttc_cmd = TTC_BCMD_TEST_ENABLE or ( tp_pending > 0 and bunch_ctr = tp_bctr(i) ) );
        txData_s(i)(3) <= txData_reg(i)(3);
        txData_s(i)(2) <= bo2sl(ttc_cmd = TTC_BCMD_OC0);
        txData_s(i)(1) <= txData_reg(i)(1);
        txData_s(i)(0) <= bo2sl(bunch_ctr = bunchctr_fire(i));
        
        if (elink_fire_old = '0' and elink_fire(i) = '1') then
          pipeout := '1' & txData_reg(i)(63 downto 32) ; 
        end if; 
        txData_s(i)(6) <= pipeout(32);
      elsif txdata_sel(i) = '1' then
        txData_s(i) <= txData_reg(i); 
      end if;
      
      if tp_pending > 0 then
        if bunch_ctr = tp_bctr(i) then tp_pending := tp_pending - 1;
        end if;
      elsif (tp_fire_old = '0' and tp_fire(i) = '1') then
        tp_pending := 1;
      end if;
      
      pipeout := pipeout(31 downto 0) & '0';
      elink_fire_old := elink_fire(i) ;
      tp_fire_old := tp_fire(i) ;
      -- Receive data
      
      last_elink_byte(i) <= rxData_s(i)(7 downto 0);
      if erase_elink_word(i) = '1' then
        last_elink_word(i) <= (others =>'0');
        pipein := (others => '0');  
      elsif pipein(32) = '1' then
        last_elink_word(i) <= pipein(31 downto 0);  
        pipein := (others => '0');
      end if;
      
      unlock_time(i) <= unlock_time(i) + bo2int(header_lock_v(i) = '0');
      unlock_ctr(i)  <= unlock_ctr(i)  + bo2int(header_lock_v(i) = '0' and last_header_lock = '1');
      last_header_lock := header_lock_v(i);
      
      pipein := pipein(31 downto 0) & rxData_s(i)(to_integer(conf_elink_sample_bit));
    end if;
  end process;
end generate;


tm7_gbt_inst : entity work.tm7_gbt
generic map( 
  GENERATE_ILAS => x"FFF",
  REMAP_RX => (
    1 =>  (mgt => 119, ch => 4 ),
    2 =>  (mgt => 119, ch => 1 ),
    3 =>  (mgt => 119, ch => 2 ),
    4 =>  (mgt => 119, ch => 3 ),
    5 =>  (mgt => 118, ch => 4 ),
    6 =>  (mgt => 118, ch => 3 ),
    7 =>  (mgt => 117, ch => 1 ),
    8 =>  (mgt => 118, ch => 2 ),
    9 =>  (mgt => 117, ch => 2 ),
    10 => (mgt => 118, ch => 1 ),
    11 => (mgt => 117, ch => 3 ),
    12 => (mgt => 117, ch => 4 )
  ),
  REMAP_TX => (
    1 =>  (mgt => 117, ch => 3 ),
    2 =>  (mgt => 117, ch => 4 ),
    3 =>  (mgt => 117, ch => 2 ),
    4 =>  (mgt => 118, ch => 1 ),
    5 =>  (mgt => 117, ch => 1 ),
    6 =>  (mgt => 118, ch => 2 ),
    7 =>  (mgt => 119, ch => 4 ),
    8 =>  (mgt => 118, ch => 3 ),
    9 =>  (mgt => 119, ch => 3 ),
    10 => (mgt => 118, ch => 4 ),
    11 => (mgt => 119, ch => 2 ),
    12 => (mgt => 119, ch => 1 )
  )
 )
port map(   
  -- Clocks   --
  clk40           => clk40_dly, -- Frame clock
  clk_fr          => clk100,
-- MGT117
  mgt117_clkp    => mgt117_clkp,
  mgt117_clkn    => mgt117_clkn,
  mgt117_txp     => mgt117_txp,
  mgt117_txn     => mgt117_txn,
  mgt117_rxp     => mgt117_rxp,
  mgt117_rxn     => mgt117_rxn,
-- MGT118
  mgt118_clkp    => mgt118_clkp,
  mgt118_clkn    => mgt118_clkn,
  mgt118_txp     => mgt118_txp,
  mgt118_txn     => mgt118_txn,
  mgt118_rxp     => mgt118_rxp,
  mgt118_rxn     => mgt118_rxn,
-- MGT119
  mgt119_clkp    => mgt119_clkp,
  mgt119_clkn    => mgt119_clkn,
  mgt119_txp     => mgt119_txp,
  mgt119_txn     => mgt119_txn,
  mgt119_rxp     => mgt119_rxp,
  mgt119_rxn     => mgt119_rxn,
  -- ipbus --
  ipb_clk         => ipb_clk,
  ipb_rst         => ipb_rst,
  ipb_in          => ipb_to_slaves(N_SLV_TM7),
  ipb_out         => ipb_from_slaves(N_SLV_TM7),
  -- data ports --
  resetgbtfpga    => conf_resetgbtfpga,
  txFrameClk_from_txPll_o => txFrameClk_from_txPll,
  rxWordClk       => rxWordClk_s,
  rxData          => rxData_s,
  rxIsData        => rxIsData_s,
  txData          => txData_s,
  txIsData        => txIsData_s,
  header_lock     => header_lock_v,
  txFrameClk_lock => open
);

headerLock <= bo2sl( header_lock_v /= (header_lock_v'range => '0') );
txIsData_s <= (others => '0');


end architecture;