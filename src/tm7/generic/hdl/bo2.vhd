----------------------------------------------------------------------------------
-- Readout data types
-- �lvaro Navarro, CIEMAT, 2015
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package bo2 is

  type bo2sl_t is array (boolean) of std_logic;
  constant bo2sl : bo2sl_t := (false => '0', true => '1');

  type bo2int_t is array (boolean) of integer range 1 downto 0;
  constant bo2int : bo2int_t := (false => 0, true => 1);

end package;

