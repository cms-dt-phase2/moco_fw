# Modified for TM7; original in boards/mp7/base_fw/common/firmware/ucf/pins_r1.tcl
# IO pin assignments for the TM7 board

# Clock IO

set_property PACKAGE_PIN L39 [get_ports {clk40_in_p}]
set_property PACKAGE_PIN L40 [get_ports {clk40_in_n}]
set_property IOSTANDARD LVDS [get_ports {clk40_in_n}]
set_property DIFF_TERM TRUE [get_ports {clk40_in_n}]
set_property PACKAGE_PIN AV38 [get_ports {ttc_in_n}]
set_property IOSTANDARD LVDS [get_ports {ttc_in_n}]
set_property DIFF_TERM TRUE [get_ports {ttc_in_n}]
set_property PACKAGE_PIN AB7 [get_ports eth_clkn]
set_property IOSTANDARD LVCMOS18 [get_ports {clk_100}]
set_property PACKAGE_PIN AJ32 [get_ports {clk_100}]

# MGT refclk
#set_property PACKAGE_PIN G9 [get_ports {refclkn[0]}]

# Front panel LEDs
set_property SLEW SLOW [get_ports {led*}]
set_property IOSTANDARD LVCMOS18 [get_ports {led*}]
set_property PACKAGE_PIN ah31 [get_ports {led1_yellow}]
set_property PACKAGE_PIN aj31 [get_ports {led1_green}]
set_property PACKAGE_PIN aj35 [get_ports {led1_red}]
set_property PACKAGE_PIN aj33 [get_ports {led1_orange}]
set_property PACKAGE_PIN an34 [get_ports {led2_yellow}]
set_property PACKAGE_PIN an33 [get_ports {led2_green}]
set_property PACKAGE_PIN ap35 [get_ports {led2_red}]
set_property PACKAGE_PIN ap37 [get_ports {led2_orange}]

# Minipod enable
set_property IOSTANDARD LVCMOS33 [get_ports {reset_mp_*x_hr}]
set_property PACKAGE_PIN bb34 [get_ports {reset_mp_rx_hr}]
set_property PACKAGE_PIN bb31 [get_ports {reset_mp_tx_hr}]

# Geographical address
set_property IOSTANDARD LVCMOS18 [get_ports {GA*}]
set_property PACKAGE_PIN AF36 [get_ports {GA[0]}]
set_property PACKAGE_PIN AF35 [get_ports {GA[1]}]
set_property PACKAGE_PIN AD36 [get_ports {GA[2]}]
set_property PACKAGE_PIN AD37 [get_ports {GA[3]}]

# Dipswitch
set_property IOSTANDARD LVCMOS18 [get_ports {dipswitch*}]
set_property PACKAGE_PIN V30 [get_ports {dipswitch[0]}]
set_property PACKAGE_PIN V31 [get_ports {dipswitch[1]}]
set_property PACKAGE_PIN T29 [get_ports {dipswitch[2]}]
set_property PACKAGE_PIN T30 [get_ports {dipswitch[3]}]
set_property PACKAGE_PIN W30 [get_ports {dipswitch[4]}]
set_property PACKAGE_PIN W31 [get_ports {dipswitch[5]}]
set_property PACKAGE_PIN V29 [get_ports {dipswitch[6]}]
set_property PACKAGE_PIN U29 [get_ports {dipswitch[7]}]

# Readout pins
#set_property PACKAGE_PIN AH28 [get_ports SEL1_MGT_CLK_18]
#set_property IOSTANDARD LVCMOS18 [get_ports SEL1_MGT_CLK_18]

# GBT BANK 117
set_property PACKAGE_PIN K7 [get_ports {mgt117_clkn}] 
set_property PACKAGE_PIN K8 [get_ports {mgt117_clkp}]

set_property PACKAGE_PIN P7 [get_ports {mgt117_rxn[0]}]
set_property PACKAGE_PIN N5 [get_ports {mgt117_rxn[1]}]
set_property PACKAGE_PIN L5 [get_ports {mgt117_rxn[2]}]
set_property PACKAGE_PIN J5 [get_ports {mgt117_rxn[3]}]
set_property PACKAGE_PIN P8 [get_ports {mgt117_rxp[0]}]
set_property PACKAGE_PIN N6 [get_ports {mgt117_rxp[1]}]
set_property PACKAGE_PIN L6 [get_ports {mgt117_rxp[2]}]
set_property PACKAGE_PIN J6 [get_ports {mgt117_rxp[3]}]

set_property PACKAGE_PIN N1 [get_ports {mgt117_txn[0]}]
set_property PACKAGE_PIN M3 [get_ports {mgt117_txn[1]}]
set_property PACKAGE_PIN L1 [get_ports {mgt117_txn[2]}]
set_property PACKAGE_PIN K3 [get_ports {mgt117_txn[3]}]
set_property PACKAGE_PIN N2 [get_ports {mgt117_txp[0]}]
set_property PACKAGE_PIN M4 [get_ports {mgt117_txp[1]}]
set_property PACKAGE_PIN L2 [get_ports {mgt117_txp[2]}]
set_property PACKAGE_PIN K4 [get_ports {mgt117_txp[3]}]

# GBT BANK 118
set_property PACKAGE_PIN E9 [get_ports {mgt118_clkn}] 
set_property PACKAGE_PIN E10 [get_ports {mgt118_clkp}]

set_property PACKAGE_PIN H7 [get_ports {mgt118_rxn[0]}]
set_property PACKAGE_PIN G5 [get_ports {mgt118_rxn[1]}]
set_property PACKAGE_PIN F7 [get_ports {mgt118_rxn[2]}]
set_property PACKAGE_PIN E5 [get_ports {mgt118_rxn[3]}]
set_property PACKAGE_PIN H8 [get_ports {mgt118_rxp[0]}]
set_property PACKAGE_PIN G6 [get_ports {mgt118_rxp[1]}]
set_property PACKAGE_PIN F8 [get_ports {mgt118_rxp[2]}]
set_property PACKAGE_PIN E6 [get_ports {mgt118_rxp[3]}]

set_property PACKAGE_PIN J1 [get_ports {mgt118_txn[0]}]
set_property PACKAGE_PIN H3 [get_ports {mgt118_txn[1]}]
set_property PACKAGE_PIN G1 [get_ports {mgt118_txn[2]}]
set_property PACKAGE_PIN F3 [get_ports {mgt118_txn[3]}]
set_property PACKAGE_PIN J2 [get_ports {mgt118_txp[0]}]
set_property PACKAGE_PIN H4 [get_ports {mgt118_txp[1]}]
set_property PACKAGE_PIN G2 [get_ports {mgt118_txp[2]}]
set_property PACKAGE_PIN F4 [get_ports {mgt118_txp[3]}]

# GBT BANK 119
set_property PACKAGE_PIN A9 [get_ports {mgt119_clkn}] 
set_property PACKAGE_PIN A10 [get_ports {mgt119_clkp}]

set_property PACKAGE_PIN D7 [get_ports {mgt119_rxn[0]}]
set_property PACKAGE_PIN C5 [get_ports {mgt119_rxn[1]}]
set_property PACKAGE_PIN B7 [get_ports {mgt119_rxn[2]}]
set_property PACKAGE_PIN A5 [get_ports {mgt119_rxn[3]}]
set_property PACKAGE_PIN D8 [get_ports {mgt119_rxp[0]}]
set_property PACKAGE_PIN C6 [get_ports {mgt119_rxp[1]}]
set_property PACKAGE_PIN B8 [get_ports {mgt119_rxp[2]}]
set_property PACKAGE_PIN A6 [get_ports {mgt119_rxp[3]}]

set_property PACKAGE_PIN E1 [get_ports {mgt119_txn[0]}]
set_property PACKAGE_PIN D3 [get_ports {mgt119_txn[1]}]
set_property PACKAGE_PIN C1 [get_ports {mgt119_txn[2]}]
set_property PACKAGE_PIN B3 [get_ports {mgt119_txn[3]}]
set_property PACKAGE_PIN E2 [get_ports {mgt119_txp[0]}]
set_property PACKAGE_PIN D4 [get_ports {mgt119_txp[1]}]
set_property PACKAGE_PIN C2 [get_ports {mgt119_txp[2]}]
set_property PACKAGE_PIN B4 [get_ports {mgt119_txp[3]}]


# RESET EXT PLL
set_property IOSTANDARD LVCMOS33 [get_ports {USR_2_HR}]
set_property PACKAGE_PIN AT34 [get_ports {USR_2_HR}]
set_property IOSTANDARD LVCMOS33 [get_ports {INTR_PLL1_HR}]
set_property PACKAGE_PIN AN30 [get_ports {INTR_PLL1_HR}]


