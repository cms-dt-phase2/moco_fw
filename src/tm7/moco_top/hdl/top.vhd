library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_moco_infra.all;
use work.mp7_ttc_decl.all;

entity top is
  port(
    ------------------------------------------------------------------------
    -- Ethernet
    eth_clkp, eth_clkn: in std_logic;
    eth_txp, eth_txn: out std_logic;
    eth_rxp, eth_rxn: in std_logic;
    ------------------------------------------------------------------------
    -- LEDs
    led1_yellow,led1_green,led1_red,led1_orange: out std_logic; -- on mp7 leds 1,0,10,9 {led_q(4:0)}
    led2_yellow,led2_green,led2_red,led2_orange: out std_logic; -- on mp7 leds 1,0,n,n  {not(locked and onehz),locked)
    ------------------------------------------------------------------------
    -- TTC
    clk40_in_p: in std_logic;
    clk40_in_n: in std_logic;
    ttc_in_p: in std_logic;
    ttc_in_n: in std_logic;
    ------------------------------------------------------------------------
    -- Misc
    reset_mp_rx_hr, reset_mp_tx_hr: out std_logic;
    clk_100 : in  STD_LOGIC;
    GA : in  STD_LOGIC_VECTOR(3 downto 0);
    dipswitch : in  STD_LOGIC_VECTOR(7 downto 0);
    USR_2_HR : out STD_LOGIC;
    INTR_PLL1_HR : in STD_LOGIC;
    -- SEL1_MGT_CLK_18 : out  std_logic;
    ------------------------------------------------------------------------
    -- GBT ports
    mgt117_clkp, mgt117_clkn  : in std_logic;
    mgt117_txp, mgt117_txn    : out std_logic_vector(0 to 3);
    mgt117_rxp, mgt117_rxn    : in std_logic_vector(0 to 3);
    mgt118_clkp, mgt118_clkn  : in std_logic;
    mgt118_txp, mgt118_txn    : out std_logic_vector(0 to 3);
    mgt118_rxp, mgt118_rxn    : in std_logic_vector(0 to 3);
    mgt119_clkp, mgt119_clkn  : in std_logic;
    mgt119_txp, mgt119_txn    : out std_logic_vector(0 to 3);
    mgt119_rxp, mgt119_rxn    : in std_logic_vector(0 to 3);
    -- SPI flash memory ports
    -- outSpiClk is output through STARTUPE2.USRCCLKO
    outSpiCsB           : out std_logic; -- Spi Chip Select. Connect to SPI Flash chip-select via FPGA FCS_B pin
    outSpiMosi          : out std_logic; -- Spi MOSI. Connect to SPI DQ0 pin via the FPGA D00_MOSI pin
    inSpiMiso           : in  std_logic; -- Spi MISO. Connect to SPI DQ1 pin via the FPGA D01_DIN pin
    outSpiWpB           : out std_logic; -- SPI flash write protect. Connect to SPI 'W_B/Vpp/DQ2'
    outSpiHoldB         : out std_logic  -- Connect to SPI 'HOLD_B/DQ3'. Fix to '1'.
  );

end top;

architecture rtl of top is
  
  signal clk_ipb, rst_ipb, clk40ish, clk_fr, clk40, rst40, clk100, eth_refclk: std_logic;
  signal clk40_rst, clk40_sel, clk40_lock, clk40_stop, nuke, soft_rst: std_logic;
  
  signal ipb_out_root: ipb_wbus;
  signal ipb_in_root: ipb_rbus;
  signal ipb_to_slaves: ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);

  signal board_id: std_logic_vector(31 downto 0);
  signal ttc_l1a, dist_lock, oc_flag, ec_flag, ttc_l1a_throttle, ttc_l1a_flag, ttc_l1a_dist: std_logic;
  signal ttc_cmd, ttc_cmd_dist: ttc_cmd_t;
  signal bunch_ctr_mp7: bctr_t;
  signal evt_ctr, orb_ctr: eoctr_t;
  signal tmt_sync: tmt_sync_t;
  
  signal leds: std_logic_vector(11 downto 0);
  
  signal MAC_lsb : std_logic_vector(15 downto 0) := (others=>'0');
  
  -- GBT
  signal txFrameClk_lock    : std_logic;
  signal headerLock         : std_logic;
  
begin

  BUFG_inst : BUFG
  port map ( I => clk_100, O => clk100 );

  -- Minipods enable
  reset_mp_rx_hr <= '1';
  reset_mp_tx_hr <= '1';
  
  -- Status LEDs
  led1_yellow <= leds(1);  -- not led_q(3)
  led1_green  <= leds(0);  -- not led_q(2)
  led1_red    <= leds(10); -- not led_q(1)
  led1_orange <= '0'; --leds(9);  -- not led_q(0)
  led2_yellow <= leds(4);  -- not (locked and onehz)
  led2_green  <= leds(3);  -- locked
  led2_red    <= headerLock;
  led2_orange <= txFrameClk_lock;

  -- Clocks and control IO
  infra: entity work.mp7_infra
  generic map(
    MAC_ADDR => X"080030f30000", -- mac addr: 08:00:30:F3:00:00
    IP_ADDR =>  X"c0A80169"      -- fixed ip: 192.168.1.105
  )
  port map(
    gt_clkp                 => eth_clkp,
    gt_clkn                 => eth_clkn,
    gt_txp                  => eth_txp,
    gt_txn                  => eth_txn,
    gt_rxp                  => eth_rxp,
    gt_rxn                  => eth_rxn,
    leds                    => leds,
    clk_ipb                 => clk_ipb,
    rst_ipb                 => rst_ipb,
    clk40ish                => clk40ish,
    clk_fr                  => clk_fr,
    refclk_out              => eth_refclk,
    nuke                    => nuke,
    soft_rst                => soft_rst,
    oc_flag                 => oc_flag,
    ec_flag                 => ec_flag,
    MAC_lsb                 => MAC_lsb,
    ipb_out_root            => ipb_out_root,
    ipb_in_root             => ipb_in_root
  );

  --MAC_lsb <= x"02" & "1111" & GA; -- range from 02:F0 to 02:FF <-- forces uROS slice crate regardless of microswitches
  MAC_lsb <= x"02" & "0101" & GA; -- range from 02:70 to 02:7F <-- forces Trigger slice crate regardless of microswitches

  -- ipbus address decode
	fabric: entity work.ipbus_fabric_sel
    generic map(
    	NSLV => N_SLAVES,
    	SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_out_root,
      ipb_out => ipb_in_root,
      sel => ipbus_sel_moco_infra(ipb_out_root.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );


  -- Control registers and board IO
  ctrl: entity work.mp7_ctrl
  port map(
    clk         => clk_ipb,
    rst         => rst_ipb,
    ipb_in      => ipb_to_slaves(N_SLV_CTRL),
    ipb_out     => ipb_from_slaves(N_SLV_CTRL),
    nuke        => nuke,
    soft_rst    => soft_rst,
    board_id    => board_id,
    clk40_rst   => clk40_rst,
    clk40_sel   => clk40_sel,
    clk40_lock  => clk40_lock,
    clk40_stop  => clk40_stop
  );

  -- TTC signal handling
  ttc: entity work.mp7_ttc
  port map(
    clk           => clk_ipb,
    rst           => rst_ipb,
    mmcm_rst      => clk40_rst,
    sel           => clk40_sel,
    lock          => clk40_lock,
    stop          => clk40_stop,
    ipb_in        => ipb_to_slaves(N_SLV_TTC),
    ipb_out       => ipb_from_slaves(N_SLV_TTC),
    clk40_in_p    => clk40_in_p,
    clk40_in_n    => clk40_in_n,
    clk40ish_in   => clk40ish,
    clk40         => clk40,
    clk_p         => open,
    rst_p         => open,
    clks_aux      => open,
    rsts_aux      => open,
    rst40         => rst40,
    ttc_in_p      => ttc_in_p,
    ttc_in_n      => ttc_in_n,
    ttc_cmd       => ttc_cmd,
    ttc_cmd_dist  => ttc_cmd_dist,
    ttc_l1a       => ttc_l1a,
    ttc_l1a_flag  => ttc_l1a_flag,
    ttc_l1a_dist  => ttc_l1a_dist,
    l1a_throttle  => ttc_l1a_throttle,
    dist_lock     => dist_lock,
    bunch_ctr     => bunch_ctr_mp7,
    evt_ctr       => evt_ctr,
    orb_ctr       => orb_ctr,
    oc_flag       => oc_flag,
    ec_flag       => ec_flag,
    tmt_sync      => tmt_sync,
    monclk        => "000"
  );

  dist_lock <= '1';
  ttc_l1a_throttle <= '1';
  --    SEL1_MGT_CLK_18 <= '0';    

  -- GBT
  gbt : entity work.moco_gbt
  port map(   
    -- Clocks   --
    clk40           => clk40, -- Frame clock
    clk100          => clk100,
    -- MGT117(GTH) --
    mgt117_clkp    => mgt117_clkp,
    mgt117_clkn    => mgt117_clkn,
    mgt117_txp     => mgt117_txp,
    mgt117_txn     => mgt117_txn,
    mgt117_rxp     => mgt117_rxp,
    mgt117_rxn     => mgt117_rxn,
    -- MGT118(GTH) --
    mgt118_clkp    => mgt118_clkp,
    mgt118_clkn    => mgt118_clkn,
    mgt118_txp     => mgt118_txp,
    mgt118_txn     => mgt118_txn,
    mgt118_rxp     => mgt118_rxp,
    mgt118_rxn     => mgt118_rxn,
    -- MGT119(GTH) --
    mgt119_clkp    => mgt119_clkp,
    mgt119_clkn    => mgt119_clkn,
    mgt119_txp     => mgt119_txp,
    mgt119_txn     => mgt119_txn,
    mgt119_rxp     => mgt119_rxp,
    mgt119_rxn     => mgt119_rxn,
    -- ipbus --
    ipb_clk       => clk_ipb,
    ipb_rst       => rst_ipb,
    ipb_in        => ipb_to_slaves(N_SLV_GBT),
    ipb_out       => ipb_from_slaves(N_SLV_GBT),
    -- data ports --
    ttc_cmd           => ttc_cmd,
    bunch_ctr         => bunch_ctr_mp7,
    headerLock        => headerLock,
    txFrameClk_lock   => txFrameClk_lock,
    mgtrefclk_rst     => USR_2_HR
  );

  tm7_common : entity work.tm7_common
  port map(      
    -- ipbus --
    ipb_clk       => clk_ipb,
    ipb_rst       => rst_ipb,
    ipb_in        => ipb_to_slaves(N_SLV_COMMON),
    ipb_out       => ipb_from_slaves(N_SLV_COMMON),
    clk40MHz      => clk40,
    clk_locked    => leds(3)
  );


  --------------------------------------------------------------------------------
  -- Spi Programmer controlled through IpBus
  tm7_ipbus_spiprogrammer: entity work.IpBus_SpiProgrammer
  generic map(
    FW_ID  => x"BEAF0001" -- TODO: tbv is there any other FW ID constant somewhere ?
  )
  port map(
    ------------------------------------------------------------------------
    -- IpBus
    clk     => clk_ipb,   -- clock: in the following assumes 125/4 MHz
    rst     => rst_ipb,
    ipb_in  => ipb_to_slaves(N_SLV_IPBUS_SPI_PROGRAMMER),
    ipb_out => ipb_from_slaves(N_SLV_IPBUS_SPI_PROGRAMMER),
    ------------------------------------------------------------------------
    -- SPI flash memory ports
    -- outSpiClk is output through STARTUPE2.USRCCLKO
    outSpiCsB   => outSpiCsB,
    outSpiMosi  => outSpiMosi,
    inSpiMiso   => inSpiMiso,
    outSpiWpB   => outSpiWpB,
    outSpiHoldB => outSpiHoldB
    ------------------------------------------------------------------------
    -- leds        => open  -- status Leds during programming phase (optional)
    ------------------------------------------------------------------------
  );


end rtl;
