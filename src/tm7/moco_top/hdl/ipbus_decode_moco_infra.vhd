-- Address decode logic for ipbus fabric
-- 
-- This file has been AUTOGENERATED from the address table - do not hand edit
-- 
-- We assume the synthesis tool is clever enough to recognise exclusive conditions
-- in the if statement.
-- 
-- Dave Newbold, February 2011

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

package ipbus_decode_moco_infra is

  constant IPBUS_SEL_WIDTH: positive := 3;
  subtype ipbus_sel_t is std_logic_vector(IPBUS_SEL_WIDTH - 1 downto 0);
  function ipbus_sel_moco_infra(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t;

-- START automatically  generated VHDL the Tue Sep 10 13:53:03 2019 
  constant N_SLV_CTRL: integer := 0;
  constant N_SLV_TTC: integer := 1;
  constant N_SLV_GBT: integer := 2;
  constant N_SLV_COMMON: integer := 3;
  constant N_SLV_IPBUS_SPI_PROGRAMMER: integer := 4;
  constant N_SLAVES: integer := 5;
-- END automatically generated VHDL

    
end ipbus_decode_moco_infra;

package body ipbus_decode_moco_infra is

  function ipbus_sel_moco_infra(addr : in std_logic_vector(31 downto 0)) return ipbus_sel_t is
    variable sel: ipbus_sel_t;
  begin

-- START automatically  generated VHDL the Tue Sep 10 13:53:03 2019 
    if    std_match(addr, "0000---------------------0------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_CTRL, IPBUS_SEL_WIDTH)); -- ctrl / base 0x00000000 / mask 0xf0000040
    elsif std_match(addr, "0000---------------------1------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_TTC, IPBUS_SEL_WIDTH)); -- ttc / base 0x00000040 / mask 0xf0000040
    elsif std_match(addr, "0001----------------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_GBT, IPBUS_SEL_WIDTH)); -- gbt / base 0x10000000 / mask 0xf0000000
    elsif std_match(addr, "0010----------------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_COMMON, IPBUS_SEL_WIDTH)); -- common / base 0x20000000 / mask 0xf0000000
    elsif std_match(addr, "1100----------------------------") then
      sel := ipbus_sel_t(to_unsigned(N_SLV_IPBUS_SPI_PROGRAMMER, IPBUS_SEL_WIDTH)); -- ipbus_spi_programmer / base 0xc0000000 / mask 0xf0000000
-- END automatically generated VHDL

    else
        sel := ipbus_sel_t(to_unsigned(N_SLAVES, IPBUS_SEL_WIDTH));
    end if;

    return sel;

  end function ipbus_sel_moco_infra;

end ipbus_decode_moco_infra;

