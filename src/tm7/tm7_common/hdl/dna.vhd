--=================================================================================================--
-- Entity to read out the FPGA DNA identifier             
-- By Javier Sastre Alvaro
--=================================================================================================--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity dna is
  Port (
    clk         : IN  STD_LOGIC;
    start       : IN  STD_LOGIC;
    dna         : OUT STD_LOGIC_VECTOR(63 downto 0);
    ready       : OUT STD_LOGIC
  );
end dna;

architecture Behavioral of dna is

  signal dout_s  : STD_LOGIC := '0';
  signal read_s  : STD_LOGIC := '0';
  signal shift_s : STD_LOGIC := '0';
  signal dna_s   : STD_LOGIC_VECTOR(63 downto 0) := (others => '0');

  type dna_gen_StateType is (IDLE, READ_DNA, SHIFT_DNA, DNA_READY);
  signal dna_gen_State : dna_gen_StateType;

  signal shift_cntr : INTEGER range 0 to 64 := 0;

begin

  DNA_PORT_inst : DNA_PORT
   port map (
      DOUT => dout_s,   -- 1-bit DNA output data
      CLK => clk,       -- 1-bit clock input
      DIN => '0',       -- 1-bit user data input pin
      READ => read_s,   -- 1-bit input, active high load DNA, active low read
      SHIFT => shift_s  -- 1-bit input, active high shift enable
   );

  dna_gen_FSM : process(clk)
  begin
    if rising_edge(clk) then
      case dna_gen_State is
        when IDLE =>
          ready <= '0';
          read_s <= '0';
          shift_s <= '0';
          if (start = '1') then
            dna_gen_State <= READ_DNA;
          else
            dna_gen_State <= IDLE;
          end if;
        when READ_DNA =>
          ready <= '0';
          read_s <= '1';
          shift_s <= '0';
          dna_gen_State <= SHIFT_DNA;
        when SHIFT_DNA =>
          shift_cntr <= shift_cntr + 1;
          ready <= '0';
          read_s <= '0';
          if (shift_cntr < 58) then
            dna_s <= dna_s(62 downto 0) & dout_s;  -- put in from right
            shift_s <= '1';
            dna_gen_State <= SHIFT_DNA;
          else
            dna_gen_State <= DNA_READY;
          end if;
        when DNA_READY =>
          ready <= '1';
          read_s <= '0';
          shift_s <= '0';
      end case;
    end if;
  end process dna_gen_FSM;

  dna <= dna_s;

end Behavioral;
