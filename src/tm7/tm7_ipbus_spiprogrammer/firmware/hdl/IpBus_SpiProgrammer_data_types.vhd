library IEEE;
use IEEE.STD_LOGIC_1164.all;

package IpBus_SpiProgrammer_data_types is

    -- Compile-time only
    function to_std_logic(inBoolean: boolean) return std_logic;

end package IpBus_SpiProgrammer_data_types;

package body IpBus_SpiProgrammer_data_types is

  function to_std_logic(inBoolean: boolean) return std_logic is
  begin
    if inBoolean then
      return('1');
    else
      return('0');
    end if;
  end function To_Std_Logic;

end package body IpBus_SpiProgrammer_data_types;

