#-------------------------------------------------------------------------------
#-- Physical Constraints for the Remote Reprogramming of the SPI Flash Memory --
#--  via IPBUS.                                                               --
#--                                                                           --
#-- Fabio Montecassiano - INFN Padova                                         --
#-- v 1.0  September 2015                                                     --
#-------------------------------------------------------------------------------

#------------- Interface with the Spi Flash memory -----------------------------
#
#---------- Outputs
#---- outSpiCsB: Connect to SPI Flash chip-select via FPGA FCS_B pin
set_property PACKAGE_PIN AL36    [get_ports {outSpiCsB}]
set_property IOSTANDARD LVCMOS18 [get_ports {outSpiCsB}]

#---- outSpiMosi: Connect to SPI DQ0 pin via the FPGA D00_MOSI pin
set_property PACKAGE_PIN AM36    [get_ports {outSpiMosi}]
set_property IOSTANDARD LVCMOS18 [get_ports {outSpiMosi}]

#---- outSpiWpB: Connect to SPI 'W_B/Vpp/DQ2'
set_property PACKAGE_PIN AJ36    [get_ports {outSpiWpB}]
set_property IOSTANDARD LVCMOS18 [get_ports {outSpiWpB}]

#-- outSpiHoldB: Connect to SPI 'HOLD_B/DQ3'. Fixed to '1'.
set_property PACKAGE_PIN AJ37    [get_ports {outSpiHoldB}]
set_property IOSTANDARD LVCMOS18 [get_ports {outSpiHoldB}]

#---------- Inputs
#---- inSpiMiso: Connect to SPI DQ1 pin via the FPGA D01_DIN pin
set_property PACKAGE_PIN AN36    [get_ports inSpiMiso]
set_property IOSTANDARD LVCMOS18 [get_ports inSpiMiso]


#-- Leds output ----------------------------------------------------------------
#-------- Memo from IDE for _future_ upg
# net leds(0) loc = ah31 | iostandard = lvcmos18 | slew=slow;  # yellow
# net leds(1) loc = aj31 | iostandard = lvcmos18 | slew=slow;  # green
# net leds(2) loc = aj33 | iostandard = lvcmos18 | slew=slow;  # orange
# net leds(3) loc = aj35 | iostandard = lvcmos18 | slew=slow;  # red

# net leds(4) loc = an34 | iostandard = lvcmos18 | slew=slow; 
# net leds(5) loc = an33 | iostandard = lvcmos18 | slew=slow; 
# net leds(6) loc = ap37 | iostandard = lvcmos18 | slew=slow; 
# net leds(7) loc = ap35 | iostandard = lvcmos18 | slew=slow; 

create_pblock spi_prog
add_cells_to_pblock [get_pblocks spi_prog] [get_cells -quiet [list tm7_ipbus_spiprogrammer]]
resize_pblock [get_pblocks spi_prog] -add {CLOCKREGION_X0Y1}

