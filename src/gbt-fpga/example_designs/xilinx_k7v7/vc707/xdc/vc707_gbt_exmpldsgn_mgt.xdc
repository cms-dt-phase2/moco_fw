##===================================================================================================##
##========================= Xilinx design constraints (XDC) information =============================##
##===================================================================================================##
##
## Company:               CERN
## Engineer:              Julian Mendez (julian.mendez@cern.ch)
##
## Project Name:          GBT-FPGA
##
## Target Device:         VC707 (Virtex 7 dev. kit)
## Tool version:          Vivado 2014.4
##
## Version:               4.0
##
## Description:
##
## Versions history:      DATE         VERSION   AUTHOR              DESCRIPTION
##
##                        17/12/2014   1.0       Julian Mendez       First .xdc definition
##
## Additional Comments:
##
##===================================================================================================##
##===================================================================================================##

##===================================================================================================##
##====================================  TIMING CLOSURE  =============================================##
##===================================================================================================##

##=================================================##
## MGT PCS to PMA rxslide constraint               ##
##=================================================##
##set_property RXSLIDE_MODE "PMA" [get_cells -hier -filter {NAME =~ */xlx_k7v7_mgt_ip/xlx_k7v7_mgt_ip_init/xlx_k7v7_mgt_ip_i}]
set_property RXSLIDE_MODE PMA [get_cells -hier -filter {NAME =~ *gbt_inst*gthe2_i}]




set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/cnter[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/cnter[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/cnter[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/cnter[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/wr_ptr_reg[3]_0[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/ongoing}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/tx_start_read_i}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/wr}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/E[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/write_o_reg_0[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/D[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/Q[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/Deserializer_inst/tx_start_write_i}]


set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rd_ptr_reg__0[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/word_in_mem_size_reg[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/wr}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rd_ptr_reg__0[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[3]_56[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[1]_58[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rd_ptr_reg__0[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/word_in_mem_size_reg[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_rd_i}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_empty_o}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rd_ptr_reg__0[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[10]_49[3]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[5]_54[4]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[6]_53[7]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[7]_52[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/word_in_mem_size_reg[0]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/word_in_mem_size_reg[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[5]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[0]_59[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[2]_57[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[9]_50[1]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[4]_55[2]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/mem_arr_reg[8]_51[6]}]
set_property MARK_DEBUG false [get_nets {gbt/tm7_gbt_inst/gen_sc[2].gen_ila_if.gbtsc_inst/ic_inst/rx_inst/ic_rx_fifo_inst/rx_data_from_gbtx_o[6]}]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
