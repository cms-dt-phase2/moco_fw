-------------------------------------------------------
--! @file
--! @author Julian Mendez <julian.mendez@cern.ch> (CERN - EP-ESE-BE)
--! @version 6.0
--! @brief GBT-FPGA IP - Device specific transceiver
-------------------------------------------------------

--! IEEE VHDL standard library:
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

--! Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

--! Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;

--! @brief MGT - Transceiver
--! @details 
--! The MGT module provides the interface to the transceivers to send the GBT-links via
--! high speed links (@4.8Gbps)
entity mgt is
   generic (
      NUM_LINKS                                      : integer := 1;
      TX_OPTIMIZATION                                : integer range 0 to 1 := STANDARD;
      RX_OPTIMIZATION                                : integer range 0 to 1 := STANDARD;
      TX_ENCODING                                    : integer range 0 to 1 := GBT_FRAME;
      RX_ENCODING                                    : integer range 0 to 1 := GBT_FRAME
   );                                                
   port ( 
      --=============--
      -- Clocks      --
      --=============--
      MGT_REFCLK_i                 : in  std_logic;
      
      MGT_RXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);
      MGT_TXUSRCLK_o               : out std_logic_vector(1 to NUM_LINKS);
      
      --=============--
      -- Resets      --
      --=============--
      MGT_TXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);
      MGT_RXRESET_i                : in  std_logic_vector(1 to NUM_LINKS);
      
      --=============--
      -- Status      --
      --=============--
      MGT_TXREADY_o                : out std_logic_vector(1 to NUM_LINKS);
      MGT_RXREADY_o                : out std_logic_vector(1 to NUM_LINKS);

      RX_HEADERLOCKED_o            : out std_logic_vector(1 to NUM_LINKS);
      RX_HEADERFLAG_o              : out std_logic_vector(1 to NUM_LINKS);
        MGT_RSTCNT_o                 : out gbt_reg8_A(1 to NUM_LINKS);
      
      --==============--
      -- Control      --
      --==============--
      MGT_AUTORSTEn_i              : in  std_logic_vector(1 to NUM_LINKS);
      MGT_AUTORSTONEVEN_i          : in  std_logic_vector(1 to NUM_LINKS);
        
      --==============--
      -- Data         --
      --==============--
      MGT_USRWORD_i                : in  word_mxnbit_A(1 to NUM_LINKS);
      MGT_USRWORD_o                : out word_mxnbit_A(1 to NUM_LINKS);
      
      --=============================--
      -- Device specific connections --
      --=============================--
      MGT_DEVSPEC_i                : in  mgtDeviceSpecific_i_R;
      MGT_DEVSPEC_o                : out mgtDeviceSpecific_o_R
   
   );
end mgt;

--! @brief MGT - Transceiver
--! @details The MGT module implements all the logic required to send the GBT frame on high speed
--! links: resets modules for the transceiver, Tx PLL and alignement logic to align the received word with the 
--! GBT frame header.
architecture structural of mgt is
   --================================ Signal Declarations ================================--

   --==============================--
   -- RX phase alignment (bitslip) --
   --==============================--
   
   signal run_to_rxBitSlipControl                     : std_logic_vector         (1 to NUM_LINKS);
   signal rxBitSlip_from_rxBitSlipControl             : std_logic_vector         (1 to NUM_LINKS);
   signal rxBitSlip_to_gtx                            : std_logic_vector         (1 to NUM_LINKS);   
   signal resetGtxRx_from_rxBitSlipControl            : std_logic_vector         (1 to NUM_LINKS);   
   signal resetGtxTx_from_rxBitSlipControl            : std_logic_vector         (1 to NUM_LINKS);  
   signal done_from_rxBitSlipControl                  : std_logic_vector         (1 to NUM_LINKS);
   
   type rstBitSlip_FSM_t                 is (idle, reset_tx, reset_rx);
   signal mgtRst_from_bitslipCtrl       : std_logic_vector(1 to NUM_LINKS);
   type rstBitSlip_FSM_t_A              is array (natural range <>) of rstBitSlip_FSM_t; 
   signal rstBitSlip_FSM                : rstBitSlip_FSM_t_A(1 to NUM_LINKS);
      
   signal rx_reset_sig                                  : std_logic_vector(1 to NUM_LINKS);
   signal tx_reset_sig                                  : std_logic_vector(1 to NUM_LINKS);
  
   signal rx_wordclk_sig                         : std_logic_vector(1 to NUM_LINKS);
   signal tx_wordclk_sig                         : std_logic;
   
   signal rxoutclk_sig                           : std_logic_vector(1 to NUM_LINKS);
   signal txoutclk_sig                           : std_logic_vector(1 to NUM_LINKS);
   
   signal rx_reset_done                          : std_logic_vector(1 to NUM_LINKS);
   signal tx_reset_done                          : std_logic_vector(1 to NUM_LINKS);
       
   signal rxfsm_reset_done                          : std_logic_vector(1 to NUM_LINKS);
   signal txfsm_reset_done                          : std_logic_vector(1 to NUM_LINKS);
                                                                                    
   signal rxResetDone_r3                              : std_logic_vector         (1 to NUM_LINKS);
   signal txResetDone_r2                              : std_logic_vector         (1 to NUM_LINKS);
   signal rxResetDone_r2                              : std_logic_vector         (1 to NUM_LINKS);
   signal txResetDone_r                               : std_logic_vector         (1 to NUM_LINKS);   
   signal rxResetDone_r                               : std_logic_vector         (1 to NUM_LINKS);
   
   signal MGT_USRWORD_s                : gbt_reg40_A(1 to NUM_LINKS);
   signal bitSlipCmd_to_bitSlipCtrller : std_logic_vector(1 to NUM_LINKS);
   signal ready_from_bitSlipCtrller    : std_logic_vector(1 to NUM_LINKS);

   signal rx_headerlocked_s            : std_logic_vector(1 to NUM_LINKS);
   signal rx_bitslipIsEven_s           : std_logic_vector(1 to NUM_LINKS);
          
   component minipod 
       port
       (
           SYSCLK_IN                               : in   std_logic;
           SOFT_RESET_TX_IN                        : in   std_logic;
           SOFT_RESET_RX_IN                        : in   std_logic;
           DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
           GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
           GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
           GT0_DATA_VALID_IN                       : in   std_logic;
       
           --_________________________________________________________________________
           --GT0  (X0Y0)
           --____________________________CHANNEL PORTS________________________________
           --------------------------------- CPLL Ports -------------------------------
           gt0_cpllfbclklost_out                   : out  std_logic;
           gt0_cplllock_out                        : out  std_logic;
           gt0_cplllockdetclk_in                   : in   std_logic;
           gt0_cpllreset_in                        : in   std_logic;
           -------------------------- Channel - Clocking Ports ------------------------
           gt0_gtrefclk0_in                        : in   std_logic;
           gt0_gtrefclk1_in                        : in   std_logic;
           ---------------------------- Channel - DRP Ports  --------------------------
           gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
           gt0_drpclk_in                           : in   std_logic;
           gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
           gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
           gt0_drpen_in                            : in   std_logic;
           gt0_drprdy_out                          : out  std_logic;
           gt0_drpwe_in                            : in   std_logic;
           --------------------------- Digital Monitor Ports --------------------------
           gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
           ------------------------------- Loopback Ports -----------------------------
           gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
           --------------------- RX Initialization and Reset Ports --------------------
           gt0_eyescanreset_in                     : in   std_logic;
           gt0_rxuserrdy_in                        : in   std_logic;
           -------------------------- RX Margin Analysis Ports ------------------------
           gt0_eyescandataerror_out                : out  std_logic;
           gt0_eyescantrigger_in                   : in   std_logic;
           ------------------ Receive Ports - FPGA RX Interface Ports -----------------
           gt0_rxusrclk_in                         : in   std_logic;
           gt0_rxusrclk2_in                        : in   std_logic;
           ------------------ Receive Ports - FPGA RX interface Ports -----------------
           gt0_rxdata_out                          : out  std_logic_vector(39 downto 0);
           --------------------------- Receive Ports - RX AFE -------------------------
           gt0_gthrxp_in                           : in   std_logic;
           ------------------------ Receive Ports - RX AFE Ports ----------------------
           gt0_gthrxn_in                           : in   std_logic;
           ------------------- Receive Ports - RX Buffer Bypass Ports -----------------
           gt0_rxphmonitor_out                     : out  std_logic_vector(4 downto 0);
           gt0_rxphslipmonitor_out                 : out  std_logic_vector(4 downto 0);
           --------------------- Receive Ports - RX Equalizer Ports -------------------
           gt0_rxdfelpmreset_in                    : in   std_logic;
           gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
           gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
           --------------- Receive Ports - RX Fabric Output Control Ports -------------
           gt0_rxoutclk_out                        : out  std_logic;
           gt0_rxoutclkfabric_out                  : out  std_logic;
           ------------- Receive Ports - RX Initialization and Reset Ports ------------
           gt0_gtrxreset_in                        : in   std_logic;
           gt0_rxpmareset_in                       : in   std_logic;
           ----------------- Receive Ports - RX Polarity Control Ports ----------------
           gt0_rxpolarity_in                       : in   std_logic;
           ---------------------- Receive Ports - RX gearbox ports --------------------
           gt0_rxslide_in                          : in   std_logic;
           -------------- Receive Ports -RX Initialization and Reset Ports ------------
           gt0_rxresetdone_out                     : out  std_logic;
           ------------------------ TX Configurable Driver Ports ----------------------
           gt0_txpostcursor_in                     : in   std_logic_vector(4 downto 0);
           gt0_txprecursor_in                      : in   std_logic_vector(4 downto 0);
           --------------------- TX Initialization and Reset Ports --------------------
           gt0_gttxreset_in                        : in   std_logic;
           gt0_txuserrdy_in                        : in   std_logic;
           ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
           gt0_txusrclk_in                         : in   std_logic;
           gt0_txusrclk2_in                        : in   std_logic;
           --------------- Transmit Ports - TX Configurable Driver Ports --------------
           gt0_txdiffctrl_in                       : in   std_logic_vector(3 downto 0);
           ------------------ Transmit Ports - TX Data Path interface -----------------
           gt0_txdata_in                           : in   std_logic_vector(39 downto 0);
           ---------------- Transmit Ports - TX Driver and OOB signaling --------------
           gt0_gthtxn_out                          : out  std_logic;
           gt0_gthtxp_out                          : out  std_logic;
           ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
           gt0_txoutclk_out                        : out  std_logic;
           gt0_txoutclkfabric_out                  : out  std_logic;
           gt0_txoutclkpcs_out                     : out  std_logic;
           ------------- Transmit Ports - TX Initialization and Reset Ports -----------
           gt0_txresetdone_out                     : out  std_logic;
           ----------------- Transmit Ports - TX Polarity Control Ports ---------------
           gt0_txpolarity_in                       : in   std_logic;
       
       
           --____________________________COMMON PORTS________________________________
            GT0_QPLLOUTCLK_IN  : in std_logic;
            GT0_QPLLOUTREFCLK_IN : in std_logic
       
       );
       end component;
--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--
   
   --==================================== User Logic =====================================--
   gtxLatOpt_gen: for i in 1 to NUM_LINKS generate
          
       MGT_RXUSRCLK_o(i)             <= rx_wordclk_sig(i);
       MGT_TXUSRCLK_o(i)             <= tx_wordclk_sig;
       
       MGT_TXREADY_o(i)              <= txResetDone_r2(i) and txfsm_reset_done(i);
       MGT_RXREADY_o(i)              <= rxResetDone_r3(i) and rxfsm_reset_done(i) and done_from_rxBitSlipControl(i);
       
       MGT_USRWORD_o(i)    <= MGT_USRWORD_s(i);
            
       rx_reset_sig(i)                  <= MGT_RXRESET_i(i) or resetGtxRx_from_rxBitSlipControl(i);
       tx_reset_sig(i)                  <= MGT_TXRESET_i(i) or resetGtxTx_from_rxBitSlipControl(i);
                     
       minipod_inst: minipod
            port map
            (
                SYSCLK_IN                       =>      MGT_DEVSPEC_i.reset_freeRunningClock(i),
                SOFT_RESET_TX_IN                =>      tx_reset_sig(i), --MGT_I.mgtLink(i).tx_reset,
                SOFT_RESET_RX_IN                =>      rx_reset_sig(i), --MGT_I.mgtLink(i).rx_reset or resetGtxRx_from_rxBitSlipControl(i),
                DONT_RESET_ON_DATA_ERROR_IN     =>      '1',
                GT0_TX_FSM_RESET_DONE_OUT       =>      txfsm_reset_done(i),
                GT0_RX_FSM_RESET_DONE_OUT       =>      rxfsm_reset_done(i),
                GT0_DATA_VALID_IN               =>      '1',
       
           --_________________________________________________________________________
           --GT0  (X0Y4)
           --____________________________CHANNEL PORTS________________________________
               gt0_loopback_in                 =>      MGT_DEVSPEC_i.loopBack(i),
           --------------------------------- CPLL Ports -------------------------------
               gt0_cpllfbclklost_out           =>      open,
               gt0_cplllock_out                =>      open,
               gt0_cplllockdetclk_in           =>      MGT_DEVSPEC_i.reset_freeRunningClock(i),
               gt0_cpllreset_in                =>      '0', --Internally managed by the TX startup FSM
           -------------------------- Channel - Clocking Ports ------------------------
               gt0_gtrefclk0_in                =>      MGT_REFCLK_i,
               gt0_gtrefclk1_in                =>      '0',
           ---------------------------- Channel - DRP Ports  --------------------------
               gt0_drpaddr_in                  =>      MGT_DEVSPEC_i.drp_addr(i),
               gt0_drpclk_in                   =>      MGT_DEVSPEC_i.drp_clk(i),
               gt0_drpdi_in                    =>      MGT_DEVSPEC_i.drp_di(i),
               gt0_drpdo_out                   =>      MGT_DEVSPEC_o.drp_do(i),
               gt0_drpen_in                    =>      MGT_DEVSPEC_i.drp_en(i),
               gt0_drprdy_out                  =>      MGT_DEVSPEC_o.drp_rdy(i),
               gt0_drpwe_in                    =>      MGT_DEVSPEC_i.drp_we(i),
           --------------------------- Digital Monitor Ports --------------------------
               gt0_dmonitorout_out             =>      open,
               gt0_rxphmonitor_out             =>      MGT_DEVSPEC_o.rx_phMonitor(i),
               gt0_rxphslipmonitor_out         =>      MGT_DEVSPEC_o.rx_phSlipMonitor(i),
           --------------------- RX Initialization and Reset Ports --------------------
               gt0_eyescanreset_in             =>      '0',
               gt0_rxuserrdy_in                =>      '0',
           -------------------------- RX Margin Analysis Ports ------------------------
               gt0_eyescandataerror_out        =>      open,
               gt0_eyescantrigger_in           =>      '0',
           ------------------ Receive Ports - FPGA RX Interface Ports -----------------
               gt0_rxusrclk_in                 =>      rx_wordClk_sig(i),
               gt0_rxusrclk2_in                =>      rx_wordClk_sig(i),
           ------------------ Receive Ports - FPGA RX interface Ports -----------------
               gt0_rxdata_out                  =>      MGT_USRWORD_s(i),
           --------------------------- Receive Ports - RX AFE -------------------------
               gt0_gthrxp_in                   =>      MGT_DEVSPEC_i.rx_p(i),
           ------------------------ Receive Ports - RX AFE Ports ----------------------
               gt0_gthrxn_in                   =>      MGT_DEVSPEC_i.rx_n(i),
           --------------------- Receive Ports - RX Equalizer Ports -------------------
               gt0_rxdfelpmreset_in            =>      '0',
               gt0_rxmonitorout_out            =>      open,
               gt0_rxmonitorsel_in             =>      "00",
           --------------- Receive Ports - RX Fabric Output Control Ports -------------
               gt0_rxoutclk_out                =>      rxoutclk_sig(i),
           ------------- Receive Ports - RX Initialization and Reset Ports ------------
               gt0_gtrxreset_in                =>      '0', --Internally managed by the RX startup FSM
               gt0_rxpmareset_in               =>      '0',
           ----------------- Receive Ports - RX Polarity Control Ports ----------------
               gt0_rxpolarity_in               =>      MGT_DEVSPEC_i.conf_rxPol(i),
           ---------------------- Receive Ports - RX gearbox ports --------------------
               gt0_rxslide_in                  =>      rxBitSlip_to_gtx(i),
           -------------- Receive Ports -RX Initialization and Reset Ports ------------
               gt0_rxresetdone_out             =>      rx_reset_done(i),
           ------------------------ TX Configurable Driver Ports ----------------------
               gt0_txpostcursor_in             =>      MGT_DEVSPEC_i.conf_postCursor(i),
               gt0_txprecursor_in              =>      MGT_DEVSPEC_i.conf_preCursor(i),
           --------------------- TX Initialization and Reset Ports --------------------
               gt0_gttxreset_in                =>      '0', --Internally managed by the TX startup FSM
               gt0_txuserrdy_in                =>      '0', --Internally managed by the TX startup FSM
           ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
               gt0_txusrclk_in                 =>      tx_wordClk_sig,
               gt0_txusrclk2_in                =>      tx_wordClk_sig,
           --------------- Transmit Ports - TX Configurable Driver Ports --------------
               gt0_txdiffctrl_in               =>      MGT_DEVSPEC_i.conf_diffCtrl(i),
           ------------------ Transmit Ports - TX Data Path interface -----------------
               gt0_txdata_in                   =>      MGT_USRWORD_i(i), 
           ---------------- Transmit Ports - TX Driver and OOB signaling --------------
               gt0_gthtxn_out                  =>      MGT_DEVSPEC_o.tx_n(i),
               gt0_gthtxp_out                  =>      MGT_DEVSPEC_o.tx_p(i),
           ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
               gt0_txoutclk_out                =>      txoutclk_sig(i),
               gt0_txoutclkfabric_out          =>      open,
               gt0_txoutclkpcs_out             =>      open,
           ------------- Transmit Ports - TX Initialization and Reset Ports -----------
               gt0_txresetdone_out             =>      tx_reset_done(i),
           ----------------- Transmit Ports - TX Polarity Control Ports ---------------
               gt0_txpolarity_in               =>      MGT_DEVSPEC_i.conf_txPol(i),
       
           --____________________________COMMON PORTS________________________________
            GT0_QPLLOUTCLK_IN  => '0',
            GT0_QPLLOUTREFCLK_IN => '0' 
       );
       
       rxWordClkBufg: bufg
           port map (
                 O                                        => rx_wordClk_sig(i), 
                 I                                        => rxoutclk_sig(i)
           ); 
       
       ---------------------------------------------
         txResetSync: process(tx_reset_done(i), tx_wordclk_sig)
         begin
            if tx_reset_done(i) = '0' then
               txResetDone_r2(i)                      <= '0';
               txResetDone_r (i)                      <= '0';
            elsif rising_edge(tx_wordclk_sig) then
               txResetDone_r2(i)                      <= txResetDone_r(i);
               txResetDone_r (i)                      <= tx_reset_done(i);
            end if;
         end process;      
            
         rxResetSync: process(rx_reset_done(i), rx_wordclk_sig(i))
         begin
            if rx_reset_done(i) = '0' then
               rxResetDone_r3(i)                      <= '0';
               rxResetDone_r2(i)                      <= '0';
               rxResetDone_r (i)                      <= '0';
            elsif rising_edge(rx_wordclk_sig(i)) then
               rxResetDone_r3(i)                      <= rxResetDone_r2(i);
               rxResetDone_r2(i)                      <= rxResetDone_r(i);
               rxResetDone_r (i)                      <= rx_reset_done(i);
            end if;
         end process;  
                 
        --====================--
        -- RX phase alignment --
        --====================--
                  
        -- Reset on bitslip control module:
        -----------------------------------
          bitslipResetFSM_proc: PROCESS(MGT_DEVSPEC_i.reset_freeRunningClock(i), MGT_RXRESET_i(i))
             variable timer :integer range 0 to GBTRX_BITSLIP_MGT_RX_RESET_DELAY;  
          variable rstcnt: unsigned(7 downto 0);              
          begin
        
               if MGT_RXRESET_i(i) = '1' then
                    resetGtxRx_from_rxBitSlipControl(i) <= '0';
                    resetGtxTx_from_rxBitSlipControl(i) <= '0';
                    timer  := 0;
                    rstcnt := (others => '0');
                    rstBitSlip_FSM(i) <= idle;
                 
               elsif rising_edge(MGT_DEVSPEC_i.reset_freeRunningClock(i)) then
                                 
                    case rstBitSlip_FSM(i) is
                       when idle      =>       resetGtxRx_from_rxBitSlipControl(i)     <= '0';
                                               resetGtxTx_from_rxBitSlipControl(i)     <= '0';
                                            
                                               if mgtRst_from_bitslipCtrl(i) = '1' then
                                                
                                                   resetGtxRx_from_rxBitSlipControl(i) <= '1';
                                                   resetGtxTx_from_rxBitSlipControl(i) <= '1';
                                                   rstBitSlip_FSM(i) <= reset_tx;
                                                   timer := 0;
                                                   
                                                   rstcnt := rstcnt+1;
                                                
                                               end if;
                                                         
                       when reset_tx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
                                                    resetGtxTx_from_rxBitSlipControl(i)     <= '0';
                                                    rstBitSlip_FSM(i)                       <= reset_rx;
                                                    timer                                   := 0;
                                               else
                                                    timer := timer + 1;
                                               end if;
                                            
                       when reset_rx  => if timer = GBTRX_BITSLIP_MGT_RX_RESET_DELAY-1 then
                                                     resetGtxRx_from_rxBitSlipControl(i)     <= '0';
                                                     rstBitSlip_FSM(i)                       <= idle;
                                                     timer                                   := 0;
                                                else
                                                     timer := timer + 1;
                                                end if;
                                            
                    end case;
                 
                   MGT_RSTCNT_o(i)   <= std_logic_vector(rstcnt);
               end if;
            
        end process; 
           
        rxBitSlipControl: entity work.mgt_bitslipctrl 
           port map (
                   RX_RESET_I          => not(rx_reset_done(i) and rxfsm_reset_done(i)),
                   RX_WORDCLK_I        => rx_wordclk_sig(i),
                   MGT_CLK_I           => MGT_DEVSPEC_i.reset_freeRunningClock(i),
                                  
                   RX_BITSLIPCMD_i     => bitSlipCmd_to_bitSlipCtrller(i),
                   RX_BITSLIPCMD_o     => rxBitSlip_to_gtx(i),
                   
                   RX_HEADERLOCKED_i   => rx_headerlocked_s(i),
                   RX_BITSLIPISEVEN_i  => rx_bitslipIsEven_s(i),
                   RX_RSTONBITSLIP_o   => mgtRst_from_bitslipCtrl(i),
                   RX_ENRST_i          => MGT_AUTORSTEn_i(i),
                   RX_RSTONEVEN_i      => MGT_AUTORSTONEVEN_i(i),
                   
                   DONE_o              => done_from_rxBitSlipControl(i),
                   READY_o             => ready_from_bitSlipCtrller(i)
           );
               
           patternSearch: entity work.mgt_framealigner_pattsearch
               port map (
                   RX_RESET_I                          => not(rx_reset_done(i) and rxfsm_reset_done(i)),
                   RX_WORDCLK_I                        => rx_wordclk_sig(i),
                   
                   RX_BITSLIP_CMD_O                    => bitSlipCmd_to_bitSlipCtrller(i),
                   MGT_BITSLIPDONE_i                   => ready_from_bitSlipCtrller(i),
                         
                   RX_HEADER_LOCKED_O                  => rx_headerlocked_s(i),
                   RX_HEADER_FLAG_O                    => RX_HEADERFLAG_o(i),
                   RX_BITSLIPISEVEN_o                  => rx_bitslipIsEven_s(i), 
                   
                   RX_WORD_I                           => MGT_USRWORD_s(i)
               );
               
               RX_HEADERLOCKED_o(i) <= rx_headerlocked_s(i);
           end generate;
      
     txWordClkBufg: bufg
         port map (
               O                                        => tx_wordClk_sig, 
               I                                        => txoutclk_sig(1)
         ); 
             
end structural;
--=================================================================================================--
--#################################################################################################--
--=================================================================================================--